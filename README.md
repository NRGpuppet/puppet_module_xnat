# xnat

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with xnat](#setup)
    * [What xnat affects](#what-xnat-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with xnat](#beginning-with-xnat)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This is the base Puppet module for deploying a basic instance of XNAT.  Module usage
detailed in [REFERENCE.md](REFERENCE.md).

## Module Description

This module will deploy a production instance of XNAT, along with a Tomcat container and, if
specified, an Apache front-end.  The XNAT deployment expects a PostgreSQL host + database, which
this module does not deploy.  Likewise, this module does not deploy any SSL cert files.  This
module thus far only tested for the following platforms:

* CentOS/RHEL 6 with OpenJDK 7
* CentOS/RHEL 7 with OpenJDK 7
* CentOS/RHEL 7 with OpenJDK 8

Also refer to example manifests in [examples](examples).

Automated parameter documentation is available in [REFERENCE.md](REFERENCE.md).

## Setup

### What xnat affects

* XNAT deployment and configuration, whether from source or WAR
* Tomcat installation and configuration
* Apache front-end and SSL configuration, if enabled

### Setup Requirements

The XNAT module requires the some 3rd party modules, specified in [metadata.json](metadata.json).

Easiest way to install dependent modules with with the r10k tool using the Puppetfile provided with
[examples](examples) manifests.

You can also install modules one at a time:
~~~
puppet module install puppetlabs-stdlib
~~~

### Beginning with xnat


The simplest way to get XNAT up and running is to install Java, a local PostgreSQL server/role/DB, and then 
declare the XNAT module.

Example manifests that do this are listed in [examples](examples).

## Usage

Automated parameter documentation is available in [REFERENCE.md](REFERENCE.md).

## Reference

MORE TDB

Here, list the classes, types, providers, facts, etc contained in your module.
This section should include all of the under-the-hood workings of your module so
people know what the module is touching on their system but don't need to mess
with things. (We are working on automating this section!)

## Limitations

MORE TDB

This is where you list OS compatibility, version compatibility, etc.

## Development

MORE TDB

### Update puppet-strings docs

First step is to install bundler and necessary gems listed in this module's Gemfile.
Very good chance you'll have to set up an rbenv, if your native Ruby stack proves
uncooperative.  It probably will be.

```
bundle install
```

Next, run the puppet-strings gem to create/update markdown and HTML docs.
```
bundle exec puppet strings generate ./manifests/*.pp
bundle exec puppet strings generate --format markdown ./manifests/*.pp
```

Finally, check the updated docs into the repo.


## Release Notes/Contributors/Etc **Optional**

MORE TDB

If you aren't using changelog, put your release notes here (though you should
consider using changelog). You may also add any additional sections you feel are
necessary or important to include here. Please use the `## ` header.
