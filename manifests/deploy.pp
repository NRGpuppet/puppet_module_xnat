# == Class: xnat::deploy
#
# Private class for XNAT deployment
#
class xnat::deploy {

  include xnat

  # Chose the build class based on version specified
  case $::xnat::version {
    '1.6': {
      class{ 'xnat::deploy::build_xnat16': }
      Class['xnat::deploy::build_xnat16'] ~> Class['xnat::deploy']
      Postgresql::Validate_db_connection['validate XNAT pgsql connection'] -> Class['xnat::deploy::build_xnat16']
    }
    '1.7': {
      class{ 'xnat::deploy::build_xnat17': }
      Class['xnat::deploy::build_xnat17'] ~> Class['xnat::deploy']
      Postgresql::Validate_db_connection['validate XNAT pgsql connection'] -> Class['xnat::deploy::build_xnat17']
    }
  }
}
