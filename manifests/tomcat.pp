# == Class: xnat::tomcat
#
# Private class for XNAT tomcat container
#
# *pkg_exception*
#  If true, only manage assets specific to tomcat installation, don't touch XNAT configuration. E.g.
#  set this true to manage tomcat package upgrage w/o touching XNAT config.
#
# TODO: Verify for cases beyond installing Tomcat7 via package on CentOS6 and CentOS7.
# TODO: Extend to support Tomcat8 (and Tomcat6?)
#
class xnat::tomcat (
  Boolean $pkg_exception = false,
  ) {

  include xnat

  $tomcat_user_real = $::xnat::tomcat_user
  $tomcat_group_real = $::xnat::tomcat_group

  # Set tomcat user home directory per XNAT version and/or value of $tomcat_user_home
  if $::xnat::tomcat_user_home {
    $tomcat_user_home_real = $::xnat::tomcat_user_home
  }
  else {
    $tomcat_user_home_real = $::xnat::version ? {
      '1.6' => $::xnat::tomcat_catalina_home,
      '1.7' => $::xnat::xnat_home_real, 
    }
  }

  # Additional java and tomcat module params can be passed via hiera
  if !$pkg_exception {
    include java
  }
  class { 'tomcat':
    user => $tomcat_user_real,
    group => $tomcat_group_real,
    catalina_home => $::xnat::tomcat_catalina_home,
  }

  # TODO: Ensure this works on all relevant CentOS versions, Ubuntu
  $tomcat_sysconfig = $::osfamily ? {
    'RedHat' => '/etc/sysconfig/tomcat',
    'Debian' => '/etc/default/tomcat7',
    default => '/etc/default/tomcat7',
  }
  $tomcat_etc_config = $::osfamily ? {
    'RedHat' => '/etc/tomcat',
    'Debian' => '/etc/tomcat7',
    default => '/etc/tomcat7',
  }

  # Note this resource may not notify if tomcat package already installed
  tomcat::install { $::xnat::tomcat_catalina_home:
    install_from_source => $::xnat::tomcat_install_from_source,
    source_url => $::xnat::tomcat_source_url,
    package_name => $::xnat::tomcat_package_name,
    package_ensure => $::xnat::tomcat_package_ensure,
    manage_user => $::xnat::tomcat_manage_user,
    manage_group => $::xnat::tomcat_manage_group,
    user => $tomcat_user_real,
    group => $tomcat_group_real,
    notify => Exec['chown catalina_base'],
  }

  # Modify Tomcat startup scripts as needed
  if (! $::xnat::tomcat_install_from_source) {
    # Tomcat7 installed from EPEL repo for RHEL/CentOS 6 requires patched init.d script.
    if ($::operatingsystemmajrelease == '6' and $::osfamily == 'RedHat' ) {
      file { '/etc/init.d/tomcat' :
        ensure => file,
        mode => '755',
        source => 'puppet:///modules/xnat/tomcat7.initd.centos6',
        require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
        subscribe => Tomcat::Install::Package["${::xnat::tomcat_package_name}"],
        before => Tomcat::Service["${::xnat::project_name}"];
      }
    }
    # Tomcat7 installed from RHEL/CentOS rpm requires drop-in config for systemd
    elsif ($::operatingsystemmajrelease == '7' and $::osfamily == 'RedHat' ) {
      file {
        "/etc/systemd/system/tomcat.service.d/":
          ensure => directory,
          require => Tomcat::Install["${::xnat::tomcat_catalina_home}"];
        "/etc/systemd/system/tomcat.service.d/xnat.conf":
          ensure => file,
          content => "# This file managed by Puppet xnat module.
[Service]
ExecStop=/usr/libexec/tomcat/server stop
User=${::tomcat::user}
Group=${::tomcat::group}
UMask=0027

[Unit]
After=autofs.service",
          require => File["/etc/systemd/system/tomcat.service.d/"],
          before => Tomcat::Service["${::xnat::project_name}"],
          subscribe => Tomcat::Install::Package["${::xnat::tomcat_package_name}"],
          notify => Exec['reload tomcat.service'];
       }
      exec { 'reload tomcat.service' :
        command => '/bin/systemctl daemon-reload',
        refreshonly => true,
        notify => Tomcat::Service["${::xnat::project_name}"];
      }
    }
  }

  # Tomcat module doesn't create some resources when installed from package
  if $::xnat::tomcat_manage_user {
    ensure_resource('user', $tomcat_user_real, {
      ensure => present,
      gid    => $tomcat_group_real,
      home   => $tomcat_user_home_real,
      before => Exec['chown catalina_base'],
      })
  }
  file_line { "tomcat_sysconfig_comment":
    line => "# This file managed by xnat Puppet module",
    path => $tomcat_sysconfig,
    after => "^# a per-service manner.*",
    require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
  }
  file_line { "tomcat_sysconfig_user":
    line => "TOMCAT_USER=\"${tomcat_user_real}\"",
    path => $tomcat_sysconfig,
    match => "^TOMCAT_USER=.*",
    require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
    before => Tomcat::Service["${::xnat::project_name}"],
  }
  if $::xnat::tomcat_uid {
    User <| title == $tomcat_user_real |> { uid => $::xnat::tomcat_uid }
  }
  if $::xnat::tomcat_manage_group {
    ensure_resource('group', $tomcat_group_real, {
      ensure => present,
      before => Exec['chown catalina_base'],
      })
  }
  file_line { "tomcat_sysconfig_group":
    line => "TOMCAT_GROUP=\"${tomcat_group_real}\"",
    path => $tomcat_sysconfig,
    match => "^TOMCAT_GROUP=.*",
    require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
    before => Tomcat::Service["${::xnat::project_name}"],
  }
  if $::xnat::tomcat_gid {
    Group <| title == $tomcat_group_real |> { gid => $::xnat::tomcat_gid }
  }
  ensure_resource('file', $::xnat::tomcat_catalina_home, {
    ensure => directory,
    owner => $tomcat_user_real,
    group => $tomcat_group_real,
    notify => Exec['chown catalina_base'],
    })
  if $::xnat::tomcat_catalina_home != $::xnat::tomcat_catalina_base {
    ensure_resource('file', $::xnat::tomcat_catalina_base, {
      ensure => directory,
      owner => $tomcat_user_real,
      group => $tomcat_group_real,
      notify => Exec['chown catalina_base'],
      })
  }

  # Set CATALINA_TMPDIR if defined
  if !$pkg_exception {
    if $::xnat::tomcat_catalina_tmpdir {
      common::mkdir_p { $::xnat::tomcat_catalina_tmpdir: }
      ensure_resource('file', $::xnat::tomcat_catalina_tmpdir, {
        ensure => directory,
        owner => $tomcat_user_real,
        group => $tomcat_group_real,
        before => Tomcat::Service["${::xnat::project_name}"],
        require => [Common::Mkdir_p["${::xnat::tomcat_catalina_tmpdir}"], Tomcat::Install["${::xnat::tomcat_catalina_home}"] ]},
      )
      file_line { "tomcat_sysconfig_tmpdir":
        line => "CATALINA_TMPDIR=\"${::xnat::tomcat_catalina_tmpdir}\"",
        path => $tomcat_sysconfig,
        match => "^CATALINA_TMPDIR=.*",
        require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
        before => Tomcat::Service["${::xnat::project_name}"],
      }
    }
  }

  # Fix/verify link to /etc/tomcat
  exec { "move old tomcat conf":
    command => "mv ${::xnat::tomcat_catalina_home}/conf ${::xnat::tomcat_catalina_home}/conf.orig",
    creates => "${::xnat::tomcat_catalina_home}/conf.orig",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    unless => "test -L ${::xnat::tomcat_catalina_home}/conf",
    require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
    before => Tomcat::Service["${::xnat::project_name}"];
  }
  ->
  file { "${::tomcat::catalina_home}/conf":
    ensure => link,
    target => $tomcat_etc_config,
  }

  # Set JAVA_HOME and JAVA_OPTS
  if !$pkg_exception {
    $_java_opts_real =  $::xnat::version ? {
      '1.6' => "-Djavax.sql.DataSource.Factory=org.apache.commons.dbcp.BasicDataSourceFactory ${::xnat::java_opts}",
      '1.7' => "-Djava.awt.headless=true -XX:+UseConcMarkSweepGC -XX:-OmitStackTraceInFastThrow -XX:+CMSIncrementalMode -XX:+CMSClassUnloadingEnabled -Dxnat.home=${::xnat::xnat_home_real} ${::xnat::java_opts}",
    }
    $java_opts_real = $::xnat::extra_java_opts ? {
      undef => $_java_opts_real,
      default => "${_java_opts_real} ${::xnat::extra_java_opts}",
    }

    $java_config_bounce_notify = $::xnat::tomcat_bounce_on_config ? {
      true => Tomcat::Service["${::xnat::project_name}"],
      default => undef
    }
    file_line { "tomcat_sysconfig_java_opts":
      line => "JAVA_OPTS=\"$java_opts_real\"",
      path => $tomcat_sysconfig,
      match => "^JAVA_OPTS=.*",
      notify => $java_config_bounce_notify,
      require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
      before => Tomcat::Instance["${::xnat::project_name}"];
    }

    if $::java::java_home {
      file_line { "tomcat_sysconfig_java_home":
        line => "JAVA_HOME=\"${::java::java_home}\"",
        path => $tomcat_sysconfig,
        match => "^JAVA_HOME=.*",
        notify => $java_config_bounce_notify,
        require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
        before => Tomcat::Instance["${::xnat::project_name}"];
      }
    }
  }

  # Deploy tomcat service
  tomcat::instance { "${::xnat::project_name}":
    catalina_base => $::xnat::tomcat_catalina_base,
    manage_service => false,
  }
  ->
  tomcat::service { "${::xnat::project_name}":
    use_jsvc     => false,
    use_init     => true,
    service_name => 'tomcat';
  }

  # Ensure correct ownership of CATALINA_BASE
  # Ignore error codes; often problems with missing log4j.jar symlinks
  exec { 'chown catalina_base':
    command => "chown -RLf ${tomcat_user_real}.${tomcat_group_real} $::xnat::tomcat_catalina_base/{.,lib,logs,logs/.,temp,work,temp/..,work/..,webapps/.} ; chown -Lf ${tomcat_user_real}.root ${::xnat::tomcat_catalina_base}/logs/.",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    returns => [0, 1],
    timeout => '18000',
    refreshonly => true,
    notify => Tomcat::Service["${::xnat::project_name}"],
  }
  if (! $::xnat::tomcat_install_from_source) {
    # Rerun if tomcat package is updated
    Tomcat::Install::Package["${::xnat::tomcat_package_name}"] ~> Exec['chown catalina_base']
  }

  # No do XNAT-specific tomcat config
  if !$pkg_exception {
    $config_bounce_notify = $::xnat::tomcat_bounce_on_config ? {
      true => Tomcat::Service["${::xnat::project_name}"],
      default => undef
    }
    
    # Define any tomcat::config::server::connector resources declared.
    if $::xnat::tomcat_connectors {
      $connector_defaults = {
        catalina_base => $::xnat::tomcat_catalina_base,
        require => Tomcat::Instance["${::xnat::project_name}"],
        before => Tomcat::Service["${::xnat::project_name}"],
        notify => $config_bounce_notify,
      }
      create_resources('tomcat::config::server::connector', $::xnat::tomcat_connectors, $connector_defaults)
    }
    
    # Define any tomcat::config::server::hosts resources declared.
    if $::xnat::tomcat_hosts {
      $host_defaults = {
        app_base => 'webapps',
        require => Tomcat::Instance["${::xnat::project_name}"],
        before => Tomcat::Service["${::xnat::project_name}"],
        notify => $config_bounce_notify,
      }
      create_resources('tomcat::config::server::host', $::xnat::tomcat_hosts, $host_defaults)
    }
    
    # Further Tomcat config depends on XNAT version
    case $::xnat::version {
      '1.6': {
        class { 'xnat::tomcat::tomcat_xnat16': }
        contain xnat::tomcat::tomcat_xnat16
        Tomcat::Install["${::xnat::tomcat_catalina_home}"] -> Class['xnat::tomcat::tomcat_xnat16']
      }
      
      '1.7': {
        class { 'xnat::tomcat::tomcat_xnat17': }
        contain xnat::tomcat::tomcat_xnat17
        Tomcat::Install["${::xnat::tomcat_catalina_home}"] -> Class['xnat::tomcat::tomcat_xnat17']
      }
    }
  }
}
