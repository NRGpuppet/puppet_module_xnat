# == Class: xnat::deploy::build_xnat17
#
# Private class for XNAT v.17 deployment from WAR
#
class xnat::deploy::build_xnat17 {

  include xnat
  include xnat::deploy
  include tomcat
  include postgresql::client
  include java
  include xnat::retrieve::war_xnat17

  # Create directories in XNAT home, etc
  $home_dirs = [ "${::xnat::xnat_home_real}/config", "${::xnat::xnat_home_real}/logs",
                 "${::xnat::xnat_home_real}/plugins", "${::xnat::xnat_home_real}/work" ]
  file { $home_dirs:
    ensure => directory,
    owner => $::xnat::tomcat_user,
    group => $::xnat::tomcat_group,
    require => File[$::xnat::xnat_home_real],
  }

  # XNAT config files
  $prefs_bounce = $::xnat::tomcat_bounce_on_update ? {
    true => Tomcat::Service["${::xnat::project_name}"],
    default => undef,
  }
  file { 
    "${::xnat::xnat_home_real}/config/xnat-conf.properties":
      ensure => file,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
      content => template('xnat/xnat-conf.properties.erb'),
      require => File["${::xnat::xnat_home_real}/config"],
      before => Tomcat::Service["${::xnat::project_name}"],
      notify => $prefs_bounce;
    "${::xnat::xnat_home_real}/config/node-conf.properties":
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
      ensure => file,
      require => File["${::xnat::xnat_home_real}/config"],
      before => Tomcat::Service["${::xnat::project_name}"],
      notify => $prefs_bounce;
  }
  file_line {
    'node_conf_props_comment':
      path => "${::xnat::xnat_home_real}/config/node-conf.properties",
      require => File["${::xnat::xnat_home_real}/config/node-conf.properties"],
      line => '# This file managed by Puppet xnat module.  Edit with care.';
    'node_conf_props_nodeid':
      path => "${::xnat::xnat_home_real}/config/node-conf.properties",
      require => File["${::xnat::xnat_home_real}/config/node-conf.properties"],
      line => "node.id=${::hostname}",
      match => '^node\.id=.*',
      replace => true;
  }
  if $::xnat::prefs_init_content {
    file { "${::xnat::xnat_home_real}/config/prefs-init.ini" :
      ensure => file,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
      content => $::xnat::prefs_init_content,
      require => File["${::xnat::xnat_home_real}/config"],
      before => Tomcat::Service["${::xnat::project_name}"],
      notify => $prefs_bounce,
    }
  }
  if $::xnat::prefs_override_content {
    file { "${::xnat::xnat_home_real}/config/prefs-override.ini" :
      ensure => file,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
      content => $::xnat::prefs_override_content,
      require => File["${::xnat::xnat_home_real}/config"],
      before => Tomcat::Service["${::xnat::project_name}"],
      notify => $prefs_bounce,
    }
  }

  $pipeline_build_cwd = $::xnat::retrieve::war_xnat17::pipeline_build_cwd

  # Pipeline config files, if specified
  if $::xnat::pipeline_config_file {
    if ($::xnat::pipeline_source_zip_url or $::xnat::pipeline_source_repo) and !($::xnat::pipeline_zip_url or $::xnat::pipeline_repo) {
      notify { "xnat module warns that specifying a pipeline.config file while also building pipeline from source may cause conflict!": }
    }
    file { "${::xnat::pipeline_path_real}/pipeline.config" :
      ensure => file,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
      content => $::xnat::pipeline_config_file,
      require => File["${::xnat::pipeline_path_real}"],
      before => Tomcat::Service["${::xnat::project_name}"],
      notify => $prefs_bounce,
    }
  }
  if $::xnat::pipeline_log4j_properties {
    if ($::xnat::pipeline_source_zip_url or $::xnat::pipeline_source_repo) and !($::xnat::pipeline_zip_url or $::xnat::pipeline_repo) {
      notify { "xnat module warns that specifying a pipeline log4j.properties file while also building pipeline from source may cause conflict!": }
    }
    file { "${::xnat::pipeline_path_real}/log4j.properties" :
      ensure => file,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
      content => $::xnat::pipeline_log4j_properties,
      require => File["${::xnat::pipeline_path_real}"],
      before => Tomcat::Service["${::xnat::project_name}"],
      notify => $prefs_bounce,
    }
  }
  if $pipeline_build_cwd {
    file { 
      "${pipeline_build_cwd}/gradle.properties" :
        ensure => file,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
        content => template('xnat/pipeline.gradle.properties.erb'),
        require => File["${pipeline_build_cwd}"],
        notify => Exec['build_pipeline'];
    }
  }

  # Pipeline build/deploy, if pipeline provided
  if $::xnat::pipeline_source_zip_url and $pipeline_build_cwd {
    $pipeline_filename = $::xnat::retrieve::war_xnat17::pipeline_filename

    exec {
      'unzip_pipeline_source':
        # This actually just removes existing pipeline source unzip
        command => "rm -rf ${::xnat::data_root}/src/pipeline_source_unzip_${::fqdn}/xnat-pipeline",
        refreshonly => true,
        notify => Exec['really_unzip_pipeline_source'];
      'really_unzip_pipeline_source':
        # Assumed pipeline zip contains top-level directory ./xnat-pipeline
        command => "unzip -qo ${::xnat::data_root}/src/${pipeline_filename}",
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        user => $::xnat::tomcat_user,
        cwd => "${::xnat::data_root}/src/pipeline_source_unzip_${::fqdn}",
        refreshonly => true,
        notify => [ File["${pipeline_build_cwd}/gradle.properties"],
                    Exec['build_pipeline'] ];
    }
  }
  if $::xnat::pipeline_zip_url {
    $pipeline_filename = $::xnat::retrieve::war_xnat17::pipeline_filename
    file {
      "${::xnat::data_root}/src/pipeline_unzip_${::fqdn}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    exec {
      'unzip_pipeline':
        # This actually just removes existing pipeline unzip
        command => "rm -rf ${::xnat::data_root}/src/pipeline_unzip_${::fqdn}/xnat-pipeline",
        refreshonly => true,
        notify => Exec['really_unzip_pipeline'];
      'really_unzip_pipeline':
        command => "unzip -qo ${::xnat::data_root}/src/${pipeline_filename}",
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        user => $::xnat::tomcat_user,
        cwd => "${::xnat::data_root}/src/pipeline_unzip_${::fqdn}",
        refreshonly => true,
        notify => Exec['rsync_pipeline'];
    }
  }
  if $::xnat::pipeline_zip_url or $::xnat::pipeline_repo {
    exec {
      'rsync_pipeline':
        command => $::xnat::retrieve::war_xnat17::pipeline_rsync_command,
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        user => $::xnat::tomcat_user,
        refreshonly => true,
        notify => Exec['xnat_deploy_war'];
    }
  }
  if ($::xnat::pipeline_source_zip_url or $::xnat::pipeline_source_repo) and $pipeline_build_cwd {
    exec {
      'build_pipeline':
        command => "${pipeline_build_cwd}/gradlew -q",
        user => $::xnat::tomcat_user,
        environment => [ "JAVA_HOME=${::xnat::java_home}" ],
        cwd => $pipeline_build_cwd,
        tries => '3', # gradle build is fragile when dependent artifacts don't download reliably
        try_sleep => '30',
        refreshonly => true,
        require => File["${pipeline_build_cwd}/gradle.properties"],
        notify => Exec['xnat_deploy_war'];
    }
  }

  # Unpack plugins, if plugins zip provided
  if $::xnat::plugins_zip_url {
    $plugins_filename = $::xnat::retrieve::war_xnat17::plugins_filename
    file {
      "${::xnat::data_root}/src/plugins_unzip_${::fqdn}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    ->
    exec {
      'unzip_plugins':
        command => "unzip -qo ${::xnat::data_root}/src/${plugins_filename}",
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        user => $::xnat::tomcat_user,
        cwd => "${::xnat::data_root}/src/plugins_unzip_${::fqdn}",
        refreshonly => true,
        notify => Exec['rsync_plugins'];
    }
  }

  # Run rysnc to copy plugins, if plugins provided
  if $::xnat::plugins_zip_url or $::xnat::plugins_repo {
     exec {
      'rsync_plugins':
        command => $::xnat::retrieve::war_xnat17::plugins_rsync_command,
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        user => $::xnat::tomcat_user,
        refreshonly => true,
        require => File["${::xnat::xnat_home_real}/plugins"],
        notify => Exec['xnat_deploy_war'];
     }
  }

  # Run plugins manager gradle script, if provided
  if $::xnat::plugins_manager_repo {
    $plugins_gradle_cwd = $::xnat::retrieve::war_xnat17::plugins_gradle_cwd
    $plugins_gradle_options = pick($::xnat::plugins_manager_gradle_options, ' ')
    exec {
      'plugins_manager_gradle':
        command => "${plugins_gradle_cwd}/gradlew ${plugins_gradle_options}",
        cwd => $plugins_gradle_cwd,
        timeout => 3600,
        logoutput => true,
        user => $::xnat::tomcat_user,
        environment => [ "JAVA_HOME=${::xnat::java_home}", "XNAT_HOME=${::xnat::xnat_home_real}" ],
        refreshonly => true,
        require => File["${::xnat::xnat_home_real}/plugins"],
        notify => Exec['xnat_deploy_war'];
    }
  }

  # TODO: Deploy a custom values for archive_path, cache_path, etc to site-config.properties
  
  # Exec resources to build and deploy WAR
  $deploy_war_bounce = $::xnat::tomcat_bounce_on_update ? {
    true => Tomcat::Service["${::xnat::project_name}"],
    default => undef,
  }
  exec {
    # Build WAR using gradlew
    'xnat_build_war':
      command => "${::xnat::data_root}/src/war_source_repo_${::fqdn}/gradlew war",
      cwd => "${::xnat::data_root}/src/war_source_repo_${::fqdn}",
      environment => [ "JAVA_HOME=${::xnat::java_home}" ],
      user => $::xnat::tomcat_user,
      refreshonly => true,
      notify => Exec['xnat_deploy_war'];
    # Deploy WAR and bounce tomcat
    'xnat_deploy_war':
# Don't rm old exploded war
#      command => "rm -rf ${::xnat::tomcat_catalina_base}/webapps/ROOT ; ${::xnat::retrieve::war_xnat17::war_cp_command}",
      command => "${::xnat::retrieve::war_xnat17::war_cp_command}",
      path    => ['/usr/bin', '/usr/sbin', '/bin'],
      user => $::xnat::tomcat_user,
      refreshonly => true,
      before => Tomcat::Service["${::xnat::project_name}"],
      notify => $deploy_war_bounce,
      require => [ File["${::xnat::xnat_home_real}/config/xnat-conf.properties"],
                   Tomcat::Install["${::xnat::tomcat_catalina_home}"] ];
  }
}
