# == Class: xnat::retrieve
#
# Private class for XNAT package retrieval
#
class xnat::retrieve {

  include xnat

  # Chose the retrieve class based on version specified
  case $::xnat::version {
    '1.6': {
      class{ 'xnat::retrieve::source_xnat16': }
      File[$::xnat::data_root] -> Class['xnat::retrieve::source_xnat16']
      File[$::xnat::pipeline_path_real] -> Class['xnat::retrieve::source_xnat16']
    }
    '1.7': {
      class{ 'xnat::retrieve::war_xnat17': }
      File[$::xnat::xnat_home_real] -> Class['xnat::retrieve::war_xnat17']
      File[$::xnat::pipeline_path_real] -> Class['xnat::retrieve::war_xnat17']
    }
  }
}
