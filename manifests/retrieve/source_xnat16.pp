# == Class: xnat::retrieve::source_xnat16
#
# Private class for retrieving XNAT v1.6 from source repos
#
class xnat::retrieve::source_xnat16 {
 
  include xnat

  $repo_options = {
    'ensure' => 'present',
    'provider' => 'git',
    'user' => $::xnat::tomcat_user,
  }

  # Retrieve XNAT artifacts from repos
  exec {
    "xnat16_clean_builder_repo":
      command => "rm -rf ${::xnat::data_root}/builder_${::fqdn}",
      refreshonly => true,
      subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
      before => File["${::xnat::data_root}/builder_${::fqdn}"],
  }
  file {
    "${::xnat::data_root}/builder_${::fqdn}":
      ensure => directory,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
  }
  $install16_builder_repo_hash = {
    "${::xnat::data_root}/builder_${::fqdn}" => $::xnat::install16_builder_source_repo,
    $::xnat::pipeline_path_real => $::xnat::install16_pipeline_source_repo,
  }
  create_resources('vcsrepo', $install16_builder_repo_hash, $repo_options)
  Vcsrepo["${::xnat::data_root}/builder_${::fqdn}"] -> File_line['project_properties_artifactory']
  Vcsrepo["${::xnat::pipeline_path_real}"] -> File_line['project_properties_artifactory']

  # Currently must patch project.properties to include direct reference to artifactoryonline.com;
  # Maven v1.0.2 is too stupid to understand 302 redirects.
  file_line {
    'project_properties_artifactory':
      path => "${::xnat::data_root}/builder_${::fqdn}/project.properties",
      match => '^maven\.repo\.remote(\s?)\=',
      line => 'maven.repo.remote=http://maven.xnat.org/xnat-maven1,http://repo1.maven.org/maven,http://mirrors.ibiblio.org/pub/mirrors/maven,http://mirrors.ibiblio.org/maven,https://nrgxnat.jfrog.io/nrgxnat/xnat-maven1';
  }
  if $::xnat::install16_project_source_repo {
    exec {
      "xnat16_clean_project_repo":
        command => "rm -rf ${::xnat::data_root}/project_${::fqdn}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => File["${::xnat::data_root}/project_${::fqdn}"],
    }
    file { "${::xnat::data_root}/project_${::fqdn}":
      ensure => directory,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,    
    }
    $install16_project_source_repo_hash = {
      "${::xnat::data_root}/project_${::fqdn}" => $::xnat::install16_project_source_repo
    }
    create_resources('vcsrepo', $install16_project_source_repo_hash, $repo_options)
  }
}
