# == Class: xnat::retrieve::war_xnat17
#
# Private class for retrieving XNAT v1.7 WAR and other artifacts
#
class xnat::retrieve::war_xnat17 {
 
  include xnat
  include wget
  include tomcat

  $fetch_dest = "${::xnat::data_root}/src/"
  $repo_options = {
    'ensure' => 'present',
    'provider' => 'git',
    'user' => $::xnat::tomcat_user, # this might get ignored when 'identity' attribute set
    'owner' => $::xnat::tomcat_user,
    'group' => $::xnat::tomcat_group,
  }

  # Retrieve XNAT WAR via method specified
  if $::xnat::xnat_war_source_repo {
    exec {
      "xnat17_clean_war_source":
        command => "rm -rf ${fetch_dest}/war_source_repo_${::fqdn}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => File["${fetch_dest}/war_source_repo_${::fqdn}"],
    }
    file {
      "${fetch_dest}/war_source_repo_${::fqdn}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    $xnat_war_source_repo_hash = {
      "${fetch_dest}/war_source_repo_${::fqdn}" => $::xnat::xnat_war_source_repo,
    }
    create_resources('vcsrepo', $xnat_war_source_repo_hash, $repo_options)
    Vcsrepo["${fetch_dest}/war_source_repo_${::fqdn}"] ~> Exec['xnat_build_war']

    $cp_war_command = "cp ${fetch_dest}/war_source_repo_${::fqdn}/build/libs/xnat-web-*.war ${::xnat::tomcat_catalina_base}/webapps/ROOT.war"    
  }
  elsif $::xnat::xnat_war_repo {
    exec {
      "xnat17_clean_war_repo":
        command => "rm -rf ${fetch_dest}/war_repo_${::fqdn}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => File["${fetch_dest}/war_repo_${::fqdn}"],
    }
    file {
      "${fetch_dest}/war_repo_${::fqdn}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    $xnat_war_repo_hash = {
      "${fetch_dest}/war_repo_${::fqdn}" => $::xnat::xnat_war_repo
    }.filter |$key, $value| { $value != undef }
    create_resources('vcsrepo', $xnat_war_repo_hash, $repo_options)
    Vcsrepo["${fetch_dest}/war_repo_${::fqdn}"] ~> Exec['xnat_deploy_war']
    
    $war_cp_command = "cp ${fetch_dest}/war_repo_${::fqdn}/*.war ${::xnat::tomcat_catalina_base}/webapps/ROOT.war"
  }
  else {
    $war_filename = join(["${::fqdn}_", basename($::xnat::xnat_war_url)])
    exec {
      "xnat17_clean_war":
        command => "rm -rf ${fetch_dest}/${war_filename}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => Wget::Fetch['xnat_war'],
    }
    wget::fetch { 'xnat_war':
      source => $::xnat::xnat_war_url,
      destination => "$fetch_dest/${war_filename}",
      execuser => $::xnat::tomcat_user,
      unless => "test -f ${fetch_dest}/${war_filename}",
      notify => Exec['xnat_deploy_war'];
    }
    
    $war_cp_command = "cp ${fetch_dest}/${war_filename} ${::xnat::tomcat_catalina_base}/webapps/ROOT.war"
  }

  # Retrieve pipeline via method specified
  if $::xnat::pipeline_source_repo {
    $pipeline_build_cwd = "${fetch_dest}/pipeline_source_repo_${::fqdn}"

    exec {
      "xnat17_clean_pipeline_source_repo":
        command => "rm -rf ${pipeline_build_cwd}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => File["${pipeline_build_cwd}"],
    }
    file {
      "${pipeline_build_cwd}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    $pipeline_source_repo_hash = {
      $pipeline_build_cwd => $::xnat::pipeline_source_repo
    }
    create_resource('vcsrepo', $pipeline_source_repo_hash, $repo_options)
    Vcsrepo["${pipeline_build_cwd}"] ~> Exec['build_pipeline']
  }
  elsif $::xnat::pipeline_repo {
    $pipeline_rsync_command = "rsync -ah --delete --exclude '.git' --exclude '.hg' --exclude '.cvs' --exclude 'pipeline.config' --exclude 'log4j.properties' --stats ${fetch_dest}/pipeline_repo_${::fqdn}/ ${::xnat::pipeline_path_real}"

    exec {
      "xnat17_clean_pipeline_repo":
        command => "rm -rf ${fetch_dest}/pipeline_repo_${::fqdn}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => File["${fetch_dest}/pipeline_repo_${::fqdn}"],
    }
    file {
      "${fetch_dest}/pipeline_repo_${::fqdn}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    $pipeline_repo_hash = {
      "${fetch_dest}/pipeline_repo_${::fqdn}" => $::xnat::pipeline_repo,
    }
    create_resources('vcsrepo', $pipeline_repo_hash, $repo_options)
    Vcsrepo["${fetch_dest}/pipeline_repo_${::fqdn}"] ~> Exec['rsync_pipeline']
  }
  elsif $::xnat::pipeline_zip_url {
    $pipeline_filename = join(["pipeline_${::fqdn}_", basename($::xnat::pipeline_zip_url)])
    # Assumed pipeline zip contains top-level directory ./xnat-pipeline
    $pipeline_rsync_command = "rsync -ah --delete --exclude 'pipeline.config' --exclude 'log4j.properties' --stats ${fetch_dest}/pipeline_unzip_${::fqdn}/xnat-pipeline/ ${::xnat::pipeline_path_real}"

    exec {
      "xnat17_clean_pipeline_zip":
        command => "rm -rf ${fetch_dest}/${pipeline_filename}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => Wget::Fetch['pipeline_file'],
    }
    wget::fetch { 'pipeline_file':
      source => $::xnat::pipeline_zip_url,
      destination => "${fetch_dest}/${pipeline_filename}",
      execuser => $::xnat::tomcat_user,
      unless => "test -f ${fetch_dest}/${pipeline_filename}",
      notify => Exec['unzip_pipeline'];
    }
  }
  elsif $::xnat::pipeline_source_zip_url {
    $pipeline_filename = join(["pipeline_source_${::fqdn}_", basename($::xnat::pipeline_source_zip_url)])
    # Assumed pipeline zip contains top-level directory ./xnat-pipeline
    $pipeline_build_cwd = "${fetch_dest}/pipeline_source_unzip_${::fqdn}/xnat-pipeline"

    exec {
      "xnat17_clean_pipeline_source_zip":
        command => "rm -rf ${fetch_dest}/${pipeline_filename}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => Wget::Fetch['pipeline_source_file'],
    }
    file {
      "${fetch_dest}/pipeline_source_unzip_${::fqdn}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group;
      "${pipeline_build_cwd}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group;
    }
    wget::fetch { 'pipeline_source_file':
      source => $::xnat::pipeline_source_zip_url,
      destination => "${fetch_dest}/${pipeline_filename}",
      execuser => $::xnat::tomcat_user,
      unless => "test -f ${fetch_dest}/${pipeline_filename}",
      notify => Exec['unzip_pipeline_source'];
    }
  }

  # Retrieve plugins via method specified
  if $::xnat::plugins_manager_repo {
    $plugins_gradle_cwd = "${fetch_dest}/plugins_manager_repo_${::fqdn}"
    exec {
      "xnat17_clean_plugins_manager_repo":
        command => "rm -rf ${plugins_gradle_cwd}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => File["${plugins_gradle_cwd}"],
    }
    file {
      "${plugins_gradle_cwd}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    $plugins_manager_repo_hash = {
      "${plugins_gradle_cwd}" => $::xnat::plugins_manager_repo
    }
    create_resources('vcsrepo', $plugins_manager_repo_hash, $repo_options)
    Vcsrepo["${plugins_gradle_cwd}"] ~> Exec['plugins_manager_gradle']
  }
  elsif $::xnat::plugins_repo {
    $plugins_rsync_command = "rsync -ah --delete --exclude '.git' --exclude '.hg' --exclude '.cvs' --stats ${fetch_dest}/plugins_repo_${::fqdn}/ ${::xnat::xnat_home_real}/plugins"

    exec {
      "xnat17_clean_plugins_repo":
        command => "rm -rf ${fetch_dest}/plugins_repo_${::fqdn}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => File["${fetch_dest}/plugins_repo_${::fqdn}"],
    }
    file {
      "${fetch_dest}/plugins_repo_${::fqdn}":
        ensure => directory,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
    }
    $plugins_repo_hash = {
      "${fetch_dest}/plugins_repo_${::fqdn}" => $::xnat::plugins_repo
    }
    create_resources('vcsrepo', $plugins_repo_hash, $repo_options)
    Vcsrepo["${fetch_dest}/plugins_repo_${::fqdn}"] ~> Exec['rsync_plugins']
  }
  elsif $::xnat::plugins_zip_url {
    $plugins_filename = join(["plugins_${::fqdn}_", basename($::xnat::plugins_zip_url)])
    # Assumed plugins zip contains top-level directory ./xnat-plugins
    $plugins_rsync_command = "rsync -ah --delete --stats ${fetch_dest}/plugins_unzip_${::fqdn}/xnat-plugins/ ${::xnat::xnat_home_real}/plugins"

    exec {
      "xnat17_clean_plugins_zip":
        command => "rm -rf ${fetch_dest}/${plugins_filename}",
        refreshonly => true,
        subscribe => File['/etc/facter/facts.d/xnat_module_started.txt'],
        before => Wget::Fetch['plugins_file'],
    }
    wget::fetch { 'plugins_file':
      source => $::xnat::plugins_zip_url,
      destination => "${fetch_dest}/${plugins_filename}",
      execuser => $::xnat::tomcat_user,
      unless => "test -f ${fetch_dest}/${plugins_filename}",
      notify => Exec['unzip_plugins'];
    }
  }

}

