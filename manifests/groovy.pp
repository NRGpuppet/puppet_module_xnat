# == Class: xnat::groovy
#
# Helper class to install Groovy from a downloaded binary zip.
#
# @param [String] binary_zip_url URL for Groovy binary zip.
# @param [String] binary_zip_md5 Optional, MD5 checksum to verify download of Groovy binary zip.
# @param [String] groovy_version The version of Groovy in the binary zip specified above.
# @param [String] groovy_home The path under which to unpack the Groovy binary zip, and to set as
#   GROOVY_HOME in the machine's environment variables.  Note that ${GROOVY_HOME}/bin will also be
#   appended to the system path.
#
# @param xnat_nopuppet If this hiera variable/fact is true, this class will do nothing.
#
# @example
#   class { 'xnat::groovy':
#     binary_zip_url => 'https://bintray.com/groovy/maven/download_file?file_path=apache-groovy-binary-2.4.4.zip',
#     groovy_version => '2.4.4',
#   }
#
# @author NRG nrg-admin@nrg.wustl.edu Copyright 2018
#
class xnat::groovy (
  String $binary_zip_url = 'https://archive.apache.org/dist/groovy/3.0.9/distribution/apache-groovy-binary-3.0.9.zip',
  String $groovy_version = '3.0.9',
  String $groovy_home = '/opt/groovy',
  Optional[String] $binary_zip_md5 = undef,
  ){

  # This class gated by $::xnat_nopuppet fact
  if ! $::xnat_nopuppet {

    include wget
    include java

    $fetch_dest = "/opt/puppet_xnat_groovy/"
    $zip_filename = basename($binary_zip_url)
    $groovy_dirname = basename($binary_zip_url, '.zip')
    $groovy_home_real = "${groovy_home}/groovy-${groovy_version}"

    ensure_packages([ 'unzip' ], {'ensure' => 'latest'})

    common::mkdir_p { $fetch_dest: }
    ->
    common::mkdir_p { $groovy_home: }
    ->
    wget::fetch { 'wget_groovy_binary_zip':
      source => $binary_zip_url,
      destination => $fetch_dest,
    }
    ->
    exec {
      'unzip_groovy_binary_zip':
        command => "unzip -qo ${fetch_dest}${zip_filename}",
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        cwd => $groovy_home,
        creates => $groovy_home_real,
        require           => [ Package['unzip'], Class['java'] ];
    }

    # Set GROOVY_HOME environment and update PATH
    file {
      "/etc/profile.d/groovy_home.sh":
        owner             => root,
        group             => root,
        mode              => "0755",
        replace           => true,
        content           => template("xnat/groovy_home.sh.erb"),
        require           => Class['java'];
      "/etc/profile.d/groovy_home.csh":
        owner             => root,
        group             => root,
        mode              => "0755",
        replace           => true,
        content           => template("xnat/groovy_home.csh.erb"),
        require           => Class['java'];
    }
  }
}
