# == Class: xnat
#
# Base class for XNAT module.  This class presently assumes OpenJDK provided via puppetlabs/java module,
# and that any SSL certs/key specified already deployed by another module/class.
#
# @param [String] db_name The name of the PostgreSQL database to use for XNAT
# @param [String] db_user The PostgreSQL database user role
# @param [Variant] db_pass The password to use for PostgreSQL database *db_name*.  It is
#   recommended this parameter be stored in encrypted Hiera for production use, or left undef and
#   database access managed via PostgreSQL HBA
# @param [Integer] db_port The PostgreSQL database port
# @param [String] db_host Required, the host of the PostgreSQL server.  If this is "localhost" or
#   $::fqdn, PostgreSQL server needs to be installed, and the database/role created per the
#   values given in *db_name*, *db_user*, and *db_pass*.  This class will fail if can't verify DB
#   connectivity.
# @param [String] java_opts The JAVA_OPTS environment variable for Tomcat, default: '-Xmx2048m -Xms512m'
#   Note this module will insert the following content before whatever is specified above for
#   *java_opts*, depending on XNAT v1.6 or v1.7:
#   v1.6: '-Djavax.sql.DataSource.Factory=org.apache.commons.dbcp.BasicDataSourceFactory'
#   v1.7: '-Djava.awt.headless=true -XX:+UseConcMarkSweepGC -XX:-OmitStackTraceInFastThrow -XX:+CMSIncrementalMode -XX:+CMSClassUnloadingEnabled -Dxnat.home=*xnat_home*'
# @param [String] extra_java_opts Extra values to append to JAVA_OPT variable, in addition
#   to anything specified in *java_opts* above.  This is a separate parameter, to permit specifying
#   JAVA_OPTS at multiple hierarchies in hiera.
# @param [Boolean] install_groovy Whether to include xnat::groovy and install Groovy.
# @param [Boolean] tomcat_manage_user If true, this module will declare user resource *tomcat_user*.
#   Also passed to tomcat::install::manage_user.
# @param [Boolean] tomcat_manage_group If true, this module will declare the group resource *tomcat_group*.
#   Also passed to tomcat::install::manage_group.
# @param [String] tomcat_user The user under which the Tomcat container process runs. This user will
#   be given ownership of the paths specified in *data_root* and *xnat_home*, along with any paths
#   under that.  Passed to tomcat::user.
# @param [Variant] tomcat_uid The uid of user under which the Tomcat container process runs.
#   Note that seting this param while *tomcat_manage_user* is false assumes that a user resource of this
#   name is declared by another module. Default: whatever the OS selects.
# @param [String] tomcat_group The group under which the Tomcat container process runs.  Passed to
#   tomcat::group.
# @param [Variant] tomcat_gid The gid of user group under which the Tomcat container process runs.
#   Note that seting this param while *tomcat_manage_group* is false assumes that a group resource of
#   this name is declared by another module. Default: whatever the OS selects.
# @param [String] tomcat_user_home Tf *tomcat_manage_user* = true, the home directory to set
#   for the Tomcat container user.  If left undef, Tomcat user home will be *tomcat_catalina_home*.
# @param [String] tomcat_catalina_home The CATALINA_HOME path for Tomcat.  When installing Tomcat from
#   package, this parameter must be set to the path created by package install.  Passed to
#   tomcat::catalina_home.
# @param [String] tomcat_catalina_base The CATALINA_BASE path for Tomcat.  When installing Tomcat from
#   package, this parameter must be set to the path created by package install.  Passed to
#   tomcat::instance::catalina_base. Default: /usr/share/tomcat
# @param [String] tomcat_catalina_tmpdir A custom CATALINA_TMPDIR path for Tomcat.
# @param [hash] tomcat_connectors A hash of resource declarations to pass to
#   create_resources('tomcat::config::server::connector', $tomcat_connectors).  See examples.
#   Default: whatever is defined by default in server.xml.
# @param [hash] tomcat_hosts A hash of resource declarations to pass to
#   create_resources('tomcat::config::server::host', $tomcat_hosts).  See examples.
#   Default: whatever is defined by default in server.xml.
# @param [String] tomcat_web_user Not yet implemented.
# @param [String] tomcat_web_pass Not yet implemented.
# @param [Boolean] tomcat_install_from_source Whether to install Tomcat from source tarball or package.
#   False = install from package. Passed to tomcat::install::install_from_source.
# @param [String] tomcat_install_source_url If *tomcat_install_from_source* = true, this is the URL for
#   the Tomcat tarball.  Passed to tomcat::install::source_url.
# @param [String] tomcat_package_name If *tomcat_install_from_source* = false, this is the
#   name of the Tomcat package. Passed to tomcat::install::package_name.
#   Default: undef (i.e. use Tomcat module default)
# @param [String] tomcat_package_ensure If *tomcat_install_from_source* = false, this is the
#   ensure attribute for the Tomcat package resource declared by Tomcat module.  E.g. present or latest.
#   Default: undef (i.e. use Tomcat module default)
# @param [Boolean] tomcat_bounce_on_config Whether to bounce tomcat on config change (i.e. JAVA_OPTS heap
#    sizes, JAVA_HOME, connectors or hosts in server.xml).
# @param [Boolean] tomcat_bounce_on_update Whether to bounce tomcat on any change in webapp, war, plugins,
#   pipeline, or prefs/config file.  If false, you'll need to bounce tomcat outside of puppet.  Also note
#   xnat::tomcat_hosts example below to disable tomcat autoDeploy on war file change.
# @param [Boolean] apache_frontend Whether to deploy an Apache front-end vhost for this XNAT instance.
#   If set to false, the apache_... params below are ignored.
# @param [String] apache_servername The ServerName parameter to use in the Apache front-end vhost.  Unless
#  you have special circumstances (e.g. frontend proxy), this should be the same value as *site_url*.
# @param [Variant] apache_ssl_cert_file The SSLCertificateFile parameter to use in the Apache front-end
#   vhost.  Note this assumes cert files are already in place. Default: false (uses puppetlabs/apache
#   module default)
# @param [Variant] apache_ssl_chain_file The SSLCertificateChainFile parameter to use in the Apache
#   front-end vhost.  Note this assumes cert files are already in place. Default: false (uses
#   puppetlabs/apache module default)
# @param [Variant] apache_ssl_key_file The SSLCertificateKeyFile parameter to use in the Apache front-end
#   vhost.  Note this assumes cert files are already in place. Default: false (uses puppetlabs/apache
#   module default)
# @param [Variant] apache_ssl_ca_file The SSLCACertificateFile parameter to use in the Apache front-end
#   vhost.  Note this assumes cert files are already in place.  Default: false (uses puppetlabs/apache
#   module default)
# @param [Array] apache_ssl_protocol The SSLProtocol parameter to use in the Apache SSL config for
#   front-end.
# @param [String] apache_ssl_cipher The SSLCipherSuite parameter to use in the Apache SSL config for
#   front-end.
# @param [Hash] apache_proxy_pass Array of hash(es) to pass to proxy_pass attribute of apache::vhost
#   resource, i.e. to pass custom params or port definitions.  Non-standard ports or timeouts defined here
#   would probably also need to be defined in *tomcat_connectors*.
# @param [Boolean] run_once If true, the module will set the local fact *xnat_nopuppet* to true after
#   first execution, disabling this module on future runs (until xnat_nopuppet is cleared or changed).
#
# @param [Variant] version The version of XNAT to deploy, "1.6" or "1.7".
# @param [String] project_name v1.7 and v1.6. The project name of the XNAT application and the Tomcat
#   webapp name.  On XNAT v1.6 deployments this is also specified as xdat.project.name in the
#   build.properties file.
# @param [String] data_root v1.7 and v1.6. The XNAT root data path, the default path under which archive,
#   pipeline, prearchive, cache, ftp, and build directories are created.  This is also where the builder
#   and pipeline source trees retrieved from repos are stored.  Additionally, for XNAT v1.7 this the
#   default path under which *xnat_home* resides.
# @param [Boolean] manage_data_root v1.7 and v1.6. Whether puppet should try to create path(s) leading
#   up to *data_root* via "mkdir -p".  Disable if this conflicts with other modules.
# @param [String] archive_path v1.7 and v1.6. A custom archive path.  For XNAT v1.6 this is specified as
#   xdat.archive.location in the build.properties file.  For XNAT v1.7, if this value is defined and 
#   *set_xnat_preferences* = true, this value will be set for archivePath in XNAT preferences upon
#   initial deploy. Default: undef (aka use XNAT default *data_root*/archive)
# @param [String] prearchive_path v1.7 and v1.6. A custom prearchive path.  For XNAT v1.6 this is
#   specified as xdat.prearchive.location in the build.properties file.  For XNAT v1.7, if 
#   *set_xnat_preferences* = true, this value will be set for prearchivePath in XNAT preferences
#   upon initial deploy. Default: undef (aka use XNAT default *data_root*/prearchive)
# @param [String] cache_path  v1.7 and v1.6. A custom cache path.  For XNAT v1.6 this is specified as
#   xdat.cache.location in the build.properties file. v1.7 and v1.6. For XNAT v1.7, if this value is
#   defined and *set_xnat_preferences* = true, this value will be set for cachePath in XNAT preferences
#   upon initial deploy. Default: undef (aka use XNAT default *data_root*/cache)
# @param [String] ftp_path v1.7 and v1.6. A custom ftp path.  For XNAT v1.6 this is specified as
#   xdat.ftp.location in the build.properties file.  For XNAT v1.7, if this value is defined and 
#   *set_xnat_preferences* = true, this value will be set for ftpPath in XNAT preferences upon
#   initial deploy. Default: undef (aka use XNAT default *data_root*/ftp)
# @param [String] build_path v1.7 and v1.6. A custom build path.  For XNAT v1.6 this is specified as
#   xdat.build.location in the build.properties file.  For XNAT v1.7, if this value is defined and 
#   *set_xnat_preferences* = true, this value will be set for buildPath in XNAT preferences upon
#   initial deploy. Default: undef (aka use XNAT default *data_root*/build)
# @param [String] pipeline_path v1.7 and v1.6. A custom pipeline path.  For XNAT v1.6, this will be the
#   destination for a checked out copy of the *install_repo_pipeline_source* repo, and it is specified as
#   xdat.pipeline.location in the build.properties file.  For XNAT v1.7, if this value is defined
#   and *set_xnat_preferences* = true, this value will be set for pipelinePath in XNAT preferences
#   upon initial deploy.  Regardless of *set_xnat_preferences*, this is also the destination of the
#   contents built from the *pipeline_zip_url* archive (unless *pipeline_zip_url* is set false).
#   Default: undef (aka use XNAT default *data_root*/pipeline)
# @param [Variant] pipeline_config_file v1.7 and v1.6.  Contents to put in a
#   *pipeline_path*/pipeline.config file.  For XNAT v1.7, setting a value for this parameter may cause
#   conflict if you also have values set (explicitly or default) for *pipeline_source_zip_url* or
#   *pipeline_source_repo*.  The pipeline gradle script may try to overwrite this file.
#   Default undef = don't care about pipeline.config.
# @param [Variant] pipeline_log4j_properties v1.7 and v1.6. Contents to put in a
#   *pipeline_path*/log4j.properties file.  For XNAT v1.7, setting a value for this parameter may cause
#   conflict if you also have values set (explicitly or default) for *pipeline_source_zip_url* or
#   *pipeline_source_repo*.  The pipeline gradle script may try to overwrite this file.
#   Default undef = don't care about log4j.properties.
# @param [String] site_url v1.7 and v1.6. The site URL. For XNAT v1.6 this is the value for xdat.url
#   in build.properties.  For XNAT v1.7, if *set_xnat_preferences* = true, this value will be set for
#   siteURL in XNAT preferences upon initial deploy.  Regardless of *set_xnat_preferences*, it will
#   also be set for xnatUrl in pipeline.gradle.properties.
# @param [String] admin_email v1.7 and v1.6. The admin email.  For XNAT v1.6 this is the value for
#   xdat.mail.admin in build.properties. For XNAT v1.7, if *set_xnat_preferences* = true, this value
#   will be set for adminEmail in XNAT preferences upon initial deploy.  Regardless of
#   *set_xnat_preferences*, it will also be set for adminEmail in pipeline.gradle.properties.
# @param [String] mail_server v1.7 and v1.6. The email server.  For XNAT v1.6 this is the value for
#   xdat.mail.server in build.properties. For XNAT v1.7, if *set_xnat_preferences* = true, this value
#   will be set for host in XNAT preferences upon initial deploy.  Regardless of
#   *set_xnat_preferences*, it will also be set for smtpServer in pipeline.gradle.properties.
# @param [Integer] mail_port v1.7 and v1.6. The email server port.  For XNAT v1.6 this is the value
#   for xdat.mail.port in build.properties. For XNAT v1.7, if *set_xnat_preferences* = true, this
#   value will be set for port in XNAT preferences upon initial deploy.  For
#   pipeline.gradle.properties this is TBD.
# @param [String] mail_username v1.7 and v1.6. The email server username.  For XNAT v1.6 this is the
#   value for xdat.mail.username in build.properties.  For XNAT v1.7, if
#   *set_xnat_preferences* = true, this value will be set for username in XNAT preferences upon
#   initial deploy.  For pipeline.gradle.properties this is TBD.
# @param [String] mail_password v1.7 and v1.6. The email server password.  For XNAT v1.6 this is the
#   value for xdat.mail.password in build.properties.  For XNAT v1.7, if
#   *set_xnat_preferences* = true, this value will be set for password in XNAT preferences upon
#   initial deploy.  For pipeline.gradle.properties this is TBD.
# @param [String] mail_prefix v1.7 and v1.6. The email subject prefix.  For XNAT v1.6 this is the
#   value for xdat.mail.prefix in build.properties.  For XNAT v1.7, if *set_xnat_preferences* = true,
#   this value will be set for emailPrefix in XNAT preferences upon initial deploy.  For
#   pipeline.gradle.properties this is TBD.
#
# @param [Srting] xnat_war_url v1.7 only. URL of the XNAT WAR file.
# @param [String] xnat_war_repo v1.7 only. Vcsrepo resource hash of repo from which XNAT WAR should
#   be retrieved, to be passed to create_resources('vcsrepo', $xnat_war_repo).  This repo is expected
#   to contain a single *.war file; other contents ignored.  Setting a value for this param overrides
#   *xnat_war_url* above. See example.
# @param [String] xnat_war_source_repo v1.7 only. Vcsrepo resource hash of repo containing source of
#   XNAT WAR.  This repo is expected to contain the gradlew script and WAR source assets.  Setting a
#   value for this param overrides *xnat_war_url* and *xnat_war_repo* above.
# @param [String] pipeline_source_zip_url v1.7 only. URL of a zip file containing XNAT pipeline source.
#   Note the zip file must include the top-level directory ./xnat-pipeline containing the gradlew
#   script and pipeline source assets. This param can also be 'false', to not download a pipeline.
#   Setting to false is useful for when a pipeline already exists on the target machine.
# @param [String] pipeline_zip_url v1.7 only. URL of a zip file containing pre-built XNAT pipeline(s)
#   to download and then unpack into *pipeline_path*.  Note the zip file must include the top-level
#   directory ./xnat-pipeline . Setting a value for this param overrides *pipeline_zip_source_url* above.
# @param [String] pipeline_repo v1.7 only Vcsrepo resource hash of repo containing pre-built XNAT
#   pipeline(s). This repo's contents would be checked out and transferred into *pipeline_path*.  This
#   repo also needs a .gitignore/.hgignore to ignore the pipeline.config and log4j.properties files.
#   Setting a value for this param overrides *pipeline_source_zip_url* and *pipeline_zip_url* above.
# @param [String] pipeline_source_repo v1.7 only Vcsrepo resource hash of repo containing XNAT pipeline
#   source. This repo is expected to contain gradlew script and pipeline source assets.  Setting a value
#   for this param overrides *pipeline_zip_url*, *pipeline_repo*, and *pipeline_source_zip_url* above.
# @param [String] plugins_zip_url v1.7 only. URL of a plugins zip file to download and then unpack into
#   *xnat_home*/plugins.
# @param [String] plugins_repo v1.7 only. Vcsrepo resource hash of repo from which XNAT plugins should be
#   retrieved.  Setting a value for this param overrides *plugins_zip_url* above.
# @param [String] plugins_manager_repo v1.7 only. Vcsrepo resource hash of repo containing a plugin
#   manager gradle script.  This repo is expected to contain a gradlew script.  Setting a value for this
#   param overrides *plugins_zip_url* and *plugins_repo* above.
# @param [String] plugins_manager_gradle_options v1.7 only. Command line options to pass to gradle script
#   provided in *plugins_manager_repo*.
#   Example: "-PpluginDir=/my/path/to/plugins -PvOptical=1.0.1-SNAPSHOT -PvWmh=1.1.0"
# @param [String] xnat_home v1.7 only. A custom XNAT home path. Where config, plugins, logs, and work are
#   stored. Default: undef (aka use XNAT default *data_root*/home)
# @param [String] site_name v1.7 only. The site title, aka site ID.  If *set_xnat_preferences* = true,
#   this value will be set for siteID in XNAT preferences upon initial deploy.  Regardless of
#   *set_xnat_preferences*, it will also be set for siteName in pipeline.gradle.properties.
# @param [Boolean] set_xnat_preferences v1.7 only. NOT IMPLEMENTED YET!  Whether to set any values in
#   XNAT preferences database table upon initial deploy, after the webapp has first started up.  Noted
#   above which parameters this affects.  Any XNAT preferences specified are only set once, upon initial
#   deployment.
# @param [Variant] prefs_init_content v1.7 only. Contents to put in a *xnat_home*/config/prefs-init.ini
#   file.  Default undef = don't care about prefs-override.ini.
# @param [Variant] prefs_override_content v1.7 only. Contents to put in a
#   *xnat_home*/config/prefs-override.ini file.
#   Default undef = don't care about prefs-override.ini.
# @param [Variant] xnat_conf_additional v1.7 only. Additional lines to put in the xnat-conf.properties
#   file beyond what's already set by templates/xant-conf.properties.erb.  See example.
#
# @param [String] modules_path v1.6 only. The path to specify for xdat.modules.location in the
#   build.properties file.  More info TBD. Default: *data_root*/modules
# @param [String] install16_builder_source_repo v1.6 only. Vcsrepo rseource hash for the repo from
#   which to retrieve the XNAT builder.
# @param [String] install16_pipeline_source_repo v1.6 only. Vcsrepo rseource hash for the repo from
#   which to retrieve the XNAT pipeline.
# @param [String] install16_project_source_repo v1.6 only. Vcsrepo resource hash for repo from which
#   to retrieve an XNAT project and store in *data_root*/project.
#
# @param xnat_nopuppet If this hiera variable/fact is true, this module will do nothing.  I.e. set this
#  fact locally to to disable puppet management of XNAT resources.  The *run_once* parameter above causes
#  this module to set this fact to true on first execution.
# @param xnat_nopuppet_tomcat_pkg_exception If this variable/fact is true, this module will still declare
#  the xnat::tomcat class, i.e. as exception to the resources otherwise disabled by *xnat_nopuppet*.  Set
#  this fact true to permit tomcat package updates on the next puppet run, and/or to have puppet clean up
#  after a package update.
#
# @example Very verbose XNAT v1.7 params in YAML
#   xnat::version: 1.7
#   xant::site_name: MyXNAT
#   xnat::db_name: dbname
#   xnat::db_user: dbuser
#   xnat::db_host: dbhost.domain.com
#   xnat::db_pass: verygoodsecret
#   xnat::java_opts: "-Xms2657m -Xmx7971m -Xmn1594m -XX:-OmitStackTraceInFastThrow -XX:MaxPermSize=256m"
#   xnat::tomcat_user: xnatuser
#   xnat::tomcat_group: xnatgroup
#   xnat::tomcat_user_home: /data/customxnat/home
#   xnat::tomcat_catalina_base: /usr/share/tomcat
#   xnat::tomcat_catalina_home: /usr/share/tomcat
#   xnat::tomcat_catalina_tmpdir: "/data/customxnat/tomcat_logs"
#   xnat::apache_proxy_pass:
#     - path: /
#       url: http://localhost:8080/
#       reverse_urls:
#         - http://localhost:8080/
#       params:
#         ttl: 600
#         timeout: 1800
#         keepalive: 'On'
#   xnat::tomcat_connectors:
#     tomcat-http:
#       port: 8080
#       protocol: HTTP/1.1
#       additional_attributes:
#         redirectPort: 8443
#         connectionTimeout: 600000
#         disableUploadTimeout: 'false'
#         connectionUploadTimeout: 36000000
#     tomcat-ajp:
#       port: 8009
#       protocol: AJP/1.3
#       additional_attributes:
#         redirectPort: 8443
#         connectionTimeout: 600000
#   xnat::tomcat_hosts:
#     localhost:
#       app_base: webapps
#       additional_attributes:
#         autoDeploy: 'false' # This stops tomcat auto-deploying on war file change
#   xnat::admin_email: xnatadmin@domain.com
#   xnat::xnat_war_repo:
#     source: https://bitbucket.org/customxnat/xnat_war
#     revision: ...
#     provider: git
#   xnat::pipeline_repo:
#     source: https://bitbucket.org/customxnat/xnat_pipelines
#     revision: ...
#     provider: git
#   xnat::prefs_override_content: |
#     [siteConfig]
#     requireLogin=false
#     siteId=MyXNAT
#   xnat::xnat_conf_additional: |
#     spring.http.multipart.max-file-size=1073741824
#     spring.http.multipart.max-request-size=1073741824
#   xnat::pipeline_log4j_properties: |
#     log4j.rootLogger=debug,nrgAppender
#     log4j.category.org.apache=WARN,nrgAppender
#     log4j.appender.nrgAppender=org.nrg.log4j.NRGFileAppender
#     log4j.appender.nrgAppender.File=/data/customxnat/pipeline_logs/pipeline.log
#     log4j.appender.nrgAppender.layout=org.apache.log4j.PatternLayout
#     log4j.appender.nrgAppender.layout.ConversionPattern=%p %t %c - %m%n
#   xnat::plugins_repo:
#     source: https://bitbucket.org/customxnat/xnat_plugins
#     revision: ...
#     provider: git
#   xnat::data_root: /data/customxnat
#   xnat::apache_ssl_cert_file: "/etc/pki/tls/certs/myhostname.crt"
#   xnat::apache_ssl_ca_file: "/etc/pki/tls/certs/myhostname_ca.crt"
#   xnat::apache_ssl_key_file: "/etc/pki/tls/private/myhostname.key"
#
# @example XNAT v1.6 params in YAML
#   xnat::version: 1.6
#   xnat::db_name: dbname
#   xnat::db_user: dbuser
#   xnat::db_host: dbhost.domain.com
#   xnat::db_pass: verygoodsecret
#   xnat::install16_builder_source_hg_repo: https://bitbucket.org/nrg/xnat_builder_1_6dev
#   xnat::install16_builder_source_hg_repo_revision: 4252ff2e68d4
#   xnat::install16_pipeline_source_hg_repo: https://bitbucket.org/nrg/pipeline_1_6dev
#   xnat::install16_pipeline_source_hg_repo_revision: 1786057e2eb8
#
# @author NRG nrg-admin@nrg.wustl.edu Copyright 2018
#
class xnat (
  Variant $version = '1.7',
  String $db_name = 'xnat',
  String $db_user = 'xnat',
  String $db_host,
  Optional[Variant] $db_pass = undef,
  Integer $db_port = 5432,
  String $java_opts = '-Xmx2048m -Xms512m',
  Optional[String] $extra_java_opts = undef,
  Boolean $install_groovy = false,
  Boolean $tomcat_manage_user = true,
  Boolean $tomcat_manage_group = true,
  String $tomcat_user = 'xnat',
  String $tomcat_group = 'xnat',
  Optional[Variant] $tomcat_uid = undef,
  Optional[Variant] $tomcat_gid = undef,
  String $tomcat_catalina_base = '/usr/share/tomcat',
  String $tomcat_catalina_home = '/usr/share/tomcat',
  Optional[String] $tomcat_user_home = undef,
  Optional[String] $tomcat_catalina_tmpdir = undef,
  Optional[Hash] $tomcat_connectors = undef,
  Optional[Hash] $tomcat_hosts = undef,
  Optional[String] $tomcat_web_user = undef,
  Optional[String] $tomcat_web_pass = undef,
  Boolean $tomcat_install_from_source = false,
  String $tomcat_install_source_url = 'http://apache.cs.utah.edu/tomcat/tomcat-8/v8.5.72/bin/apache-tomcat-8.5.72.tar.gz',
  Optional[String] $tomcat_package_name = undef,
  Optional[String] $tomcat_package_ensure = undef,
  Boolean $tomcat_bounce_on_config = true,
  Boolean $tomcat_bounce_on_update = true,
  Boolean $apache_frontend = true,
  String $apache_servername = "${::fqdn}",
  Array $apache_ssl_protocol = [ '-all', '+TLSv1', '+SSLv3'],
  String $apache_ssl_cipher = 'HIGH:MEDIUM:!aNULL:+SHA1:+MD5:+HIGH:+MEDIUM',
  Variant $apache_ssl_cert_file = false,
  Variant $apache_ssl_key_file = false,
  Variant $apache_ssl_chain_file = false,
  Variant $apache_ssl_ca_file = false,
  Optional[Variant] $apache_proxy_pass = undef,
  Boolean $run_once = false,
  String $data_root = '/data/xnat',
  Optional[String] $xnat_home = undef,
  Optional[String] $archive_path = undef,
  Optional[String] $prearchive_path = undef,
  Optional[String] $cache_path = undef,
  Optional[String] $ftp_path = undef,
  Optional[String] $build_path = undef,
  Optional[String] $pipeline_path = undef,
  Optional[String] $modules_path = undef,
  Boolean $manage_data_root = true,
  String $project_name = 'xnat',
  Integer $mail_port = 25,
  String $mail_server ='localhost',
  String $mail_username = '',
  Variant $mail_password = '',
  String $admin_email = 'root@localhost',
  String $mail_prefix = 'XNAT',
  Hash $install16_builder_source_repo = {
    'ensure' => 'present',
    'source' => 'https://bitbucket.org/nrg/xnat_builder_1_6dev',
    'provider' => 'hg',
    'revision' => '4252ff2e68d4',
  },
  Hash $install16_pipeline_source_repo = {
    'ensure' => 'present',
    'source' => 'https://bitbucket.org/nrg/pipeline_1_6dev',
    'provider' => 'hg',
    'revision' => '1786057e2eb8',
  },
  Optional[Variant] $install16_project_source_repo = undef,
  String $xnat_war_url = 'https://api.bitbucket.org/2.0/repositories/xnatdev/xnat-web/downloads/xnat-web-1.8.3.war',
  Optional[Variant] $xnat_war_repo = undef,
  Optional[Variant] $xnat_war_source_repo = undef,
  Variant $pipeline_source_zip_url = 'https://github.com/NrgXnat/xnat-pipeline-engine/releases/download/1.8.0/xnat-pipeline-1.8.0.zip',
  Optional[Variant] $pipeline_zip_url = undef,
  Optional[Variant] $pipeline_repo = undef,
  Optional[Variant] $pipeline_source_repo = undef,
  Optional[Variant] $pipeline_config_file = undef,
  Optional[Variant] $pipeline_log4j_properties = undef,
  Optional[Variant] $plugins_zip_url = undef,
  Optional[Variant] $plugins_repo = undef,
  Optional[Variant] $plugins_manager_repo = undef,
  Optional[Variant] $plugins_manager_gradle_options = undef,
  String $site_name = 'XNAT',
  String $site_url = "https://${::fqdn}",
  Optional[Variant] $prefs_init_content = undef,
  Optional[Variant] $prefs_override_content = undef,
  Optional[Variant] $xnat_conf_additional = undef,
  Boolean $set_xnat_preferences = true,
  ){

  # Populate some default params if needed
  $xnat_home_real = $xnat_home ? {
    undef => "${data_root}/home",
    default => $xnat_home,
  }
  $archive_path_real = $archive_path ? {
    undef => "${data_root}/archive",
    default => $archive_path,
  }
  $prearchive_path_real = $prearchive_path ? {
    undef => "${data_root}/prearchive",
    default => $prearchive_path,
  }
  $cache_path_real = $cache_path ? {
    undef => "${data_root}/cache",
    default => $cache_path,
  }
  $ftp_path_real = $ftp_path ? {
    undef => "${data_root}/ftp",
    default => $ftp_path,
  }
  $build_path_real = $build_path ? {
    undef => "${data_root}/build",
    default => $build_path,
  }
  $pipeline_path_real = $pipeline_path ? {
    undef => "${data_root}/pipeline",
    default => $pipeline_path,
  }
  $modules_path_real = $modules_path ? {
    undef => "${data_root}/modules",
    default => $modules_path,
  }

  ensure_resource('common::mkdir_p', '/etc/facter/facts.d')
  file { '/etc/facter/facts.d/xnat_module_started.txt':
    ensure => $xnat_nopuppet_ensure,
    content => 'xnat_module_started=true',
    require => Common::Mkdir_p['/etc/facter/facts.d'],
  }

  # This module gated by $::xnat_nopuppet fact
  if ! $::xnat_nopuppet {

    # Additional java module params can be passed via hiera
    include java

    if ($::osfamily == 'RedHat') {
      # Set SELinux mode, if specified
      include selinux
    }

    # Misc package dependencies
    $xnat_pkglist = $::osfamily ? {
      'RedHat' => [ "curl", "unzip", "zip", "mercurial" ],
      'Debian' => [ "curl", "unzip", "zip", "mercurial" ],
      default => [ ],
    }
    ensure_packages($xnat_pkglist, {'ensure' => 'latest'})
    
    # Create necesssary directories
    $xnat_dirs = $version ? {
      '1.6' => [ $data_root, $archive_path_real, $prearchive_path_real,
                 $cache_path_real, $ftp_path_real, $build_path_real,
                 $pipeline_path_real, $modules_path_real ],
      '1.7' => [ $data_root, $xnat_home_real, $archive_path_real, $prearchive_path_real,
                 $cache_path_real, $ftp_path_real, $build_path_real,
                 $pipeline_path_real, "${data_root}/src" ],
    }
    $xnat_dirs.each | String $xnat_dir | {
      if ($xnat_dir == $data_root) {
        if $manage_data_root {
          ensure_resource('common::mkdir_p', $xnat_dir)
          $dir_require = Common::Mkdir_p[$xnat_dir]
        }
        else {
          $dir_require = undef
        }
      }
      else {
        ensure_resource('common::mkdir_p', $xnat_dir)
        $dir_require = Common::Mkdir_p[$xnat_dir]
      }
      $dir_resources = {
        'ensure' => 'directory',
        'owner' => $tomcat_user,
        'group' => $tomcat_group,
        'require' => $dir_require,
      }
      ensure_resource('file', $xnat_dir, $dir_resources)
    }

    # Ensure JAVA_HOME environment var is set (since puppetlabs-java doesn't do this).
    # TODO: Generalize this so it works with Oracle JDK and other OSes
    $java_home = '/usr/lib/jvm/java'
    file {
      "/etc/profile.d/java_home.sh":
        owner		=> root,
        group		=> root,
        mode		=> "0755",
        replace		=> true,
        content		=> template("xnat/java_home.sh.erb"),
        require           => Class['java'];
      "/etc/profile.d/java_home.csh":
        owner		=> root,
        group		=> root,
        mode		=> "0755",
        replace		=> true,
        content		=> template("xnat/java_home.csh.erb"),
        require           => Class['java'];
    }

    # Install Groovy if enabled. Additional params can be passed via hiera.
    if $install_groovy {
      include xnat::groovy
    }

    # Verify Postgresql DB connectivity
    postgresql::validate_db_connection { 'validate XNAT pgsql connection':
      database_host     => $db_host,
      database_port     => $db_port,
      database_username => $db_user,
      database_password => $db_pass,
      database_name     => $db_name,
    } 
    
    # Set up Apache frontend if enabled
    if $apache_frontend {
      class { 'xnat::apache_frontend': }
    }
  }

  # Set up Tomcat container
  if ($::xnat_nopuppet_tomcat_pkg_exception or !$::xnat_nopuppet) {
    if $::xnat_nopuppet_tomcat_pkg_exception {
      notify { "xnat::tomcat class still enabled, fact xnat_nopuppet_tomcat_pkg_exception=true.": }
      $tomcat_pkg_exception = true
    }
    else {
      $tomcat_pkg_exception = false
    }
    class { 'xnat::tomcat':
      pkg_exception => $tomcat_pkg_exception
    }
  }

  # Remainder of class gated by $::xnat_nopuppet fact
  if ! $::xnat_nopuppet {

    # Retrieve XNAT assets
    class { 'xnat::retrieve': }

    # Deploy XNAT once retrieved
    class { 'xnat::deploy': }

    # Dependencies
    Postgresql::Validate_db_connection['validate XNAT pgsql connection'] -> Class['xnat::retrieve']
    Postgresql::Validate_db_connection['validate XNAT pgsql connection'] -> Class['xnat::deploy']
    Class['xnat::retrieve'] -> Class['xnat::deploy']
    Tomcat::Install["$tomcat_catalina_home"] -> Class['xnat::deploy']
    Class['xnat::deploy'] ~> Tomcat::Service["${project_name}"]
  }
  else {
    notify { "xnat module disabled, fact xnat_nopuppet=true.": }
  }

  # Set xnat_nopuppet fact if run_once is enabled
  $xnat_nopuppet_ensure = $run_once ? {
    true => 'present',
    default => 'absent',
  }
  file { '/etc/facter/facts.d/xnat_nopuppet.txt':
    ensure => $xnat_nopuppet_ensure,
    content => 'xnat_nopuppet=true',
    require => Common::Mkdir_p['/etc/facter/facts.d'],
  }

}
