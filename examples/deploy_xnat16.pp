# Example manifest to install XNAT v1.6 with OpenJDK 7 and Tomcat 7.  This manifest assumes the
# follwing (i.e. has only been tested under these conditions):
#
# - RHEL/CentOS 7
# - Besides metadata.json dependencies, thias-postfix module also installed.
# - SSL cert files are already present on the machine.

# Stand-alone puppet may need default exec resource attributes
Exec {
  path => ["/bin", "/sbin", "/usr/bin", "/usr/sbin"],
  logoutput => on_failure,
}

# Install postfix with default options
include postfix::server

# Install OpenJDK 7, overriding whatever is OS default
class { 'java':
  package => 'java-1.7.0-openjdk-devel',
  java_alternative_path => '/usr/lib/jvm/jre-1.7.0-openjdk/bin/java',
  java_alternative => 'foo', # Java module requires this param not be left undef
  java_home => '/usr/lib/jvm/java-1.7.0/';
}

# Install local PostgreSQL server, role, and DB
class { 'postgresql::globals':
  version => '9.4',
  manage_package_repo => true,
}
->
class { 'postgresql::server':
  postgres_password => 'averyverygoodsecret',
}
->
# role
postgresql::server::role {
  'xnat':
    createrole => true,
    createdb => true,
    superuser => true,
    password_hash => postgresql_password('xnat', 'anotherverygoodsecret'),
}
->
# DB
postgresql::server::db {
  'xnat':
    user => 'xnat',
    password => 'anotherverygoodsecret',
    owner => 'xnat',
}

# PostgreSQL config entries tuned for a machine with 4GB RAM
postgresql::server::config_entry {
  'max_connections':
    value => '100';
  'shared_buffers':
    value => '1000MB';
  'work_mem':
    value => '20MB';
  'maintenance_work_mem':
    value => '128MB';
  'effective_cache_size':
    value => '2000MB';
  'standard_conforming_strings':
    value => 'off';
}

# Now declare XNAT module
class { 'xnat':
  version => '1.6',
  db_user => 'xnat',
  db_name => 'xnat',
  db_host => 'localhost',
  db_pass => 'anotherverygoodsecret',
  install16_builder_source_repo => {
    'ensure' => 'present',
    'source' => 'https://bitbucket.org/nrg/xnat_builder_1_6dev',
    'provider' => 'hg',
    'revision' => '4252ff2e68d4',
  },
  install16_pipeline_source_repo => {
    'ensure' => 'present',
    'source' => 'https://bitbucket.org/nrg/pipeline_1_6dev',
    'provider' => 'hg',
    'revision' => '1786057e2eb8',
  },
  data_root => '/data/xnat',
  tomcat_user => 'xnat',
  tomcat_group => 'xnat',
  tomcat_install_from_source => false,
  tomcat_package_name => 'tomcat',
  apache_frontend => true,
  apache_servername => "${::fqdn}",
  apache_ssl_cert_file => "/etc/pki/tls/certs/${::fqdn}.crt",
  apache_ssl_ca_file => "/etc/pki/tls/certs/${::fqdn}_ca.crt",
  apache_ssl_key_file => "/etc/pki/tls/private/${::fqdn}.key";
}
