# Example manifest to install XNAT v1.7 with OpenJDK 8 and Tomcat 7.  This manifest assumes the
# follwing (i.e. has only been tested under these conditions):
#
# - RHEL/CentOS 7 
# - Besides metadata.json dependencies, thias-postfix module also installed.
# - SSL cert files are already present on the machine.

# Stand-alone puppet may need default exec resource attributes
Exec {
  path => ["/bin", "/sbin", "/usr/bin", "/usr/sbin"],
  logoutput => on_failure,
}

# Install postfix with default options
include postfix::server

# Install OpenJDK 8, overriding whatever is OS default
class { 'java':
  package => 'java-1.8.0-openjdk-devel',
  java_alternative_path => '/usr/lib/jvm/jre-1.8.0-openjdk/bin/java',
  java_alternative => 'foo', # Java module requires this param not be left undef
  java_home => '/usr/lib/jvm/java-1.8.0/';
}

# Install local PostgreSQL server, role, and DB
class { 'postgresql::globals':
  version => '9.5',
  manage_package_repo => true,
}
->
class { 'postgresql::server':
  postgres_password => 'averyverygoodsecret',
}
->
# role
postgresql::server::role {
  'xnat':
    createrole => true,
    createdb => true,
    superuser => true,
    password_hash => postgresql_password('xnat', 'anotherverygoodsecret'),
}
->
# DB
postgresql::server::db {
  'xnat':
    user => 'xnat',
    password => 'anotherverygoodsecret',
    owner => 'xnat',
}

# PostgreSQL config entries tuned for a machine with 4GB RAM
postgresql::server::config_entry {
  'max_connections':
    value => '100';
  'shared_buffers':
    value => '1000MB';
  'work_mem':
    value => '20MB';
  'maintenance_work_mem':
    value => '128MB';
  'effective_cache_size':
    value => '2000MB';
  'standard_conforming_strings':
    value => 'off';
}

# Now declare XNAT module
class { 'xnat':
  version => '1.7',
  db_user => 'xnat',
  db_name => 'xnat',
  db_host => 'localhost',
  db_pass => 'anotherverygoodsecret',
  xnat_war_url => 'https://api.bitbucket.org/2.0/repositories/xnatdev/xnat-web/downloads/xnat-web-1.7.4.1.war',
  pipeline_source_zip_url => 'https://ci.xnat.org/job/XNAT%20Pipeline%20Engine/lastSuccessfulReleaseBuild/artifact/build/libs/xnat-pipeline-1.7.4.zip',
  data_root => '/data/xnat',
  tomcat_user => 'xnat',
  tomcat_group => 'xnat',
  tomcat_install_from_source => false,
  tomcat_package_name => 'tomcat',
  apache_frontend => true,
  apache_servername => "${::fqdn}",
  apache_ssl_cert_file => "/etc/pki/tls/certs/${::fqdn}.crt",
  apache_ssl_ca_file => "/etc/pki/tls/certs/${::fqdn}_ca.crt",
  apache_ssl_key_file => "/etc/pki/tls/private/${::fqdn}.key";
}

# Use these params to build/deploy XNAT & pipeline directly from source repos instead:
#  xnat_war_source_repo => 'https://bitbucket.org/xnatdev/xnat-web',
#  pipeline_repo => 'https://bitbucket.org/xnatdev/xnat-pipeline',
